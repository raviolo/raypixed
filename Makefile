.POSIX:
CC = clang
LDLIBS = -llua -lraylib -lGL -lm -lpthread -ldl -lrt -lX11
PREFIX = /usr/local

all: raypixed
install: raypixed
	mkdir -p $(DSTDIR)$(PREFIX)/bin
	cp -f raypixed $(DSTDIR)$(PREFIX)/bin
cpconfig:
	mkdir -p $(DSTDIR)$(HOME)/.config/raypixed
	cp -f conf.lua $(DSTDIR)$(HOME)/.config/raypixed
uninstall:
	rm -f $(DSTDIR)$(PREFIX)/bin/raypixed
raypixed: main.c
	$(CC) $(LDLIBS) $< -o $@
