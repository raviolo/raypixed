/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
#include <raylib.h>
#include <stdlib.h>
#include <string.h>

enum constants{
  CONF_HOME_LEN = 50, //maximum characters for the full path to ~/.config/raypixed/conf.lua
  NCHARS = '~'-' '+1, //number of different ascii characters that GetKeyPressed can return
  SCREEN_W = 800,
  SCREEN_H = 600,
  CANVAS_W = 32, //default canvas width
  CANVAS_H = 32, //default canvas height
  COLORCELL_L = 32, //width and height of a (squared)colorcell in the palette
  CELL_SPACING = 2, //gap between colorcells
  TOOL_NAME_LEN = 7, //maximum length of a tool name
  PROMPT_MAX_LEN = 31, //maximum length of user input (when prompted for input)
  FONT_SIZE = 20, //font height
};

//the color palette is loaded from Lua and stored here
struct palette {
  int y, width;
  int numColors;
  Color *colors;
};

//the same goes for tools
struct toolbar {
  int width;
  int numTools;
  char (*tools)[TOOL_NAME_LEN + 1];
};

//this is the main interface for text input
struct prompt {
  bool active;
  char const *message;
  size_t len;
  char input[PROMPT_MAX_LEN + 1];
  void (*onenter)(char*);//gets called when the user presses enter
};

//the user can copy a rectangular region of the canvas
struct clipboard {
  Texture2D region;
  Rectangle sourceRec;//only used to flip the copied region
  int rotation;
};

struct grid {
  bool enabled;
  int width, height;
};
  
//this fuction gets called if there is an error while parsing conf.lua
static void error_lua(lua_State *L, char const *msg) {
  fprintf(stderr, "%s\n", msg);
  lua_close(L);
  exit(EXIT_FAILURE);
}

/* load the color palette from
   the table in conf.lua:
   palette = {0xffff00ff,0xff00ffff --the second color is the transparent background
   , ...}
*/
static struct palette loadPalette(lua_State *L) {
  if (lua_getglobal(L, "palette") != LUA_TTABLE)
    error_lua(L, "palette must be a table");
  
  struct palette palette = {.numColors = luaL_len(L, -1)};
  palette.colors = malloc(sizeof(Color[palette.numColors]));
  for (int i = 0; i < palette.numColors; ++i) {
    lua_geti(L, -1, i + 1);
    palette.colors[i] = GetColor(lua_tointeger(L, -1));
    lua_pop(L, 1);
  }
  lua_getglobal(L, "colorcell_size");
  palette.width = lua_tointeger(L, -1);
  if(!palette.width) palette.width = COLORCELL_L;
  lua_pop(L, 2);//pop the value of colorcell_size and the table
  return palette;
}

//Get a key from the stack to use it as index(used by loadCommands and loadTools)
static int l_tokey(lua_State *L, int idx) {
  size_t len;
  char k = tolower(*lua_tolstring(L, idx, &len));
  if (len != 1)
    error_lua(L, "no key combination allowed");
  return k - ' ';
}

/* loads keyboard commands from an table in conf.lua:
   commands = {a = function() ... end, ["+"] = ...}
*/
static void loadCommands(lua_State *L, int commandKeyRefs[NCHARS]) {
  if (lua_getglobal(L, "commands") != LUA_TTABLE)
    error_lua(L, "commands must be a table");
  lua_pushnil(L);
  while (lua_next(L, -2)) {
    if (!lua_isfunction(L, -1))
      error_lua(L, "function expected");
    if (!lua_isstring(L, -2))
      error_lua(L, "key expected");
    int k = l_tokey(L, -2);
    commandKeyRefs[k] = luaL_ref(L, LUA_REGISTRYINDEX);
  }
  lua_pop(L, 1);
}

/* loads all the tools from a table in conf.lua
   (every tool must have a callback and a name, the key to select the tool is optional):
   tools = {
   {function(mousestate, x0, y0, x1, y1) ... end, name = "name", key="z"},
   ...
   }
*/
static struct toolbar loadTools(lua_State *L, int toolKeys[NCHARS]) {
  if (lua_getglobal(L, "tools") != LUA_TTABLE)
    error_lua(L, "tools must be a table");
  struct toolbar toolbar = {.numTools = luaL_len(L, -1)};
  toolbar.tools = malloc(sizeof(char[TOOL_NAME_LEN][toolbar.numTools]));
  for (int i = 0; i < toolbar.numTools; ++i) {
    if (lua_geti(L, -1, i + 1) != LUA_TTABLE) // gets table(tool) from table(tools)
      error_lua(L, "a tool must be a table");

    if (lua_getfield(L, -1, "name") != LUA_TSTRING)
      error_lua(L, "tool has no name");
    strncpy(toolbar.tools[i], lua_tostring(L, -1), TOOL_NAME_LEN);
    toolbar.tools[i][TOOL_NAME_LEN] = 0;
    lua_pop(L, 1); // pops name

    lua_geti(L, -1, 1);                                   // gets func
    lua_rawsetp(L, LUA_REGISTRYINDEX, toolbar.tools + i); // pops func

    if (lua_getfield(L, -1, "key") == LUA_TSTRING)
      toolKeys[l_tokey(L, -1)] = i + 1; // we must remember to subtract 1 after this
    lua_pop(L, 2);                      // pop key and tool
  }
  lua_pop(L, 1);
  toolbar.width = TOOL_NAME_LEN * FONT_SIZE * 0.6;
  return toolbar;
}

//load the RenderTexture used as canvas
static RenderTexture loadCanvas(lua_State *L) {
  lua_getglobal(L, "default_width");
  int width = lua_tointeger(L, -1);
  lua_getglobal(L, "default_height");
  int height = lua_tointeger(L, -1);
  lua_pop(L, 2);
  return LoadRenderTexture(width ? width : CANVAS_W, height ? height : CANVAS_H);
}

//the globals used by the functions in the Lua API are all here
char imageName[PROMPT_MAX_LEN + 1];//displayed at the top
Vector2 mousePos, savedPos;//we need to save the mouse position for most stuff
//position the canvas at the right of the "tools area"
Vector2 canvasPos = {TOOL_NAME_LEN * FONT_SIZE, 2 * FONT_SIZE};
RenderTexture canvas;
struct clipboard clipboard;
float zoom = 16;
Color selectedColor, backgroundColor;//=the second color in the palette
struct prompt prompt;
struct grid grid;

void renameImage(char *name) {
  strcpy(imageName, name);
}

//save the image to disk
void saveImage(char *name) {
  if(name) renameImage(name);
  Image image = GetTextureData(canvas.texture);
  ImageFlipVertical(&image);
  ImageColorReplace(&image, backgroundColor, BLANK);
  ExportImage(image, imageName);
  UnloadImage(image);
}

enum undoOperator {MODIFIED, RESET, RESTORE, CLEAR};

// save and load the canvas to/from RAM
void undoCanvas(enum undoOperator op) {
  static Image savedCanvas;
  static bool modified;
  switch(op) {
  case MODIFIED:
    if(modified) break; //only save the first time
    UnloadImage(savedCanvas);
    savedCanvas = GetTextureData(canvas.texture);
    modified = true;
    break;
  case RESET:
    modified = false;
    break;
  case RESTORE: //swap canvas.texture and savedCanvas
    ImageFlipVertical(&savedCanvas);
    Texture tmp = LoadTextureFromImage(savedCanvas);
    UnloadImage(savedCanvas);
    savedCanvas = GetTextureData(canvas.texture);
    if(canvas.texture.width != tmp.width || canvas.texture.height != tmp.height) {
      UnloadRenderTexture(canvas);
      canvas = LoadRenderTexture(tmp.width, tmp.height);
    }
    BeginTextureMode(canvas);
    DrawTexture(tmp, 0, 0, WHITE);
    EndTextureMode();
    UnloadTexture(tmp);
    break;
  case CLEAR:
    UnloadImage(savedCanvas);
  }
}

//load image from disk
void openImage(char *path) {
  undoCanvas(MODIFIED);
  if(!FileExists(path)) return;
  Image tmp = LoadImage(path);
  ImageColorReplace(&tmp, BLANK, backgroundColor);
  UnloadRenderTexture(canvas);
  canvas = LoadRenderTexture(tmp.width, tmp.height);
  Texture2D texture = LoadTextureFromImage(tmp);
  UnloadImage(tmp);
  BeginTextureMode(canvas);
  DrawTexture(texture, 0, 0, WHITE);
  EndTextureMode();
  UnloadTexture(texture);
  renameImage(path);
}

/* size is "[width]x[height]".
   if an error occours, returns false.
*/
bool parseWH(char *size, int *w, int *h) {
  char *afterx = size;
  while(*afterx && *afterx++ != 'x');
  if(!afterx[0]) return false;
  *w = atoi(size);
  *h = atoi(afterx);
  if(*w <= 0 || *h <= 0) return false;
  return true;
}

//creates a new, blank image
void newImage(char *size) {
  int width, height;
  if(!parseWH(size, &width, &height)) return;
  undoCanvas(MODIFIED);
  UnloadRenderTexture(canvas);
  canvas = LoadRenderTexture(width, height);
  BeginTextureMode(canvas);
  ClearBackground(backgroundColor);
  EndTextureMode();
}

void setGrid(char *size) {
  grid.enabled = parseWH(size, &grid.width, &grid.height);
}

//ask for text input
void startPrompt(char const *msg, void(*onenter)(char*), char *defaultString) {
  prompt.message = msg;
  prompt.onenter = onenter;
  prompt.active = true;
  if(defaultString) {
    strcpy(prompt.input, defaultString);
    prompt.len = strlen(defaultString);
  }
}

//this is my implementation of the scanLineFill algorithm
#define COLOR_CMP(x) memcmp(row + x, &toFind, sizeof(Color))
void scanLineFill(Color *pixels, Color toFind, int x, int y) {
  Color *row = pixels + y * canvas.texture.width;//the line to scan
  if(y < 0 || y >= canvas.texture.height || COLOR_CMP(x)) return;
  int i = x + 1;
  //i goes all the way to the right setting the pixels to selectedColor
  while(i < canvas.texture.width && !COLOR_CMP(i)) row[i++] = selectedColor;
  //x goes all the way to the left doing the same thing
  while(x >= 0 && !COLOR_CMP(x)) row[x--] = selectedColor;
  //then x goes right until it finds i, calling scanlineFill above and below the scanned line
  for(++x; x < i; ++x) {
    scanLineFill(pixels, toFind, x, y - 1);
    scanLineFill(pixels, toFind, x, y + 1);
  }
}

void floodFill(int x, int y) {
  Image tmp = GetTextureData(canvas.texture);
  Color *pixels = GetImageData(tmp);
  UnloadImage(tmp);
  //flip the y
  y = canvas.texture.height - y - 1;
  Color toFind = pixels[x + y * canvas.texture.width];
  //if the selected color is different from the color to find
  if(memcmp(&selectedColor, &toFind, sizeof(Color))) {
    scanLineFill(pixels, toFind, x, y);
    UpdateTexture(canvas.texture, pixels);
  }
  free(pixels);
}

void swapIfGreater(int *a, int *b) {
  if(*a > *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
  }
}

#define GETXY(X,Y,i) int X = luaL_checkinteger(L, i), Y = luaL_checkinteger(L, i+1)
//screen coordinates => canvas coordinates
#define CANVASXY(X,Y) X = (X - canvasPos.x) / zoom, Y = (Y - canvasPos.y) / zoom

/* used every time a rectangle must be drawn on canvas to
   translate and find x and y, then calculate w and h
*/
Rectangle getRectOnCanvas(lua_State *L) {
  GETXY(x1, y1, 1);
  GETXY(x2, y2, 3);
  CANVASXY(x1, y1);
  CANVASXY(x2, y2);
  swapIfGreater(&x1, &x2);
  swapIfGreater(&y1, &y2);
  return (Rectangle){x1, y1, x2 - x1 + 1, y2 - y1 + 1};
}

/**************************functions used by Lua: BEGIN*/

int lrename(lua_State *L) {
  startPrompt("new name of the image", renameImage, imageName);
  return 0;
}

int lopen(lua_State *L) {
  startPrompt("image to open", openImage, imageName);
  return 0;
}

int lsave(lua_State *L) {
  if(!imageName[0])
    startPrompt("name of the image to save", saveImage, NULL);
  else saveImage(NULL);
  return 0;
}

int lnewImage(lua_State *L) {
  startPrompt("size of the new image [width]x[height]", newImage, NULL);
  return 0;
}

int lsetGrid(lua_State *L) {
  startPrompt("grid cell dimensions [width]x[height]", setGrid, NULL);
  return 0;
}

int lmove(lua_State *L) {
  GETXY(x,y,1);
  canvasPos.x += x;
  canvasPos.y += y;
  return 0;
}

int LmoveClosure(lua_State *L) {
  canvasPos.x += lua_tointeger(L, lua_upvalueindex(1));
  canvasPos.y += lua_tointeger(L, lua_upvalueindex(2));
  return 0;
}

int lmoveCommand(lua_State *L) {
  lua_pushcclosure(L, LmoveClosure, 2);
  return 1;
}   

int lzoom(lua_State *L) {
  zoom += luaL_checknumber(L, 1);
  if(zoom <= 1) zoom = 1;
  return 0;
}

int LzoomClosure(lua_State *L) {
  zoom += lua_tonumber(L, lua_upvalueindex(1));
  if(zoom <= 1) zoom = 1;
  return 0;
}

int lzoomCommand(lua_State *L) {
  lua_pushcclosure(L, LzoomClosure, 1);
  return 1;
}

int lundo(lua_State *L) {
  undoCanvas(RESTORE);
  return 0;
}

int lsaveMousePos(lua_State *L) {
  savedPos = mousePos;
  return 0;
}

int lshowRectOutline(lua_State *L) {
  EndTextureMode();
  GETXY(x1, y1, 1);
  GETXY(x2, y2, 3);
  swapIfGreater(&x1, &x2);
  swapIfGreater(&y1, &y2);
  //align to the pixel grid
  x1 -= (x1 - (int)canvasPos.x) % (int)zoom;
  x2 += zoom - (x2 - (int)canvasPos.x) % (int)zoom;
  y1 -= (y1 - (int)canvasPos.y) % (int)zoom;
  y2 += zoom - (y2 - (int)canvasPos.y) % (int)zoom;
  DrawRectangleLines(x1, y1, x2 - x1, y2 - y1, selectedColor);
  BeginTextureMode(canvas);
  return 0;
}

int ldrawRectOutline(lua_State *L) {
  undoCanvas(MODIFIED);
  DrawRectangleLinesEx(getRectOnCanvas(L), 1, selectedColor);
  return 0;
}

int ldrawRect(lua_State *L) {
  undoCanvas(MODIFIED);
  DrawRectangleRec(getRectOnCanvas(L), selectedColor);
  return 0;
}

int ldrawLine(lua_State *L) {
  undoCanvas(MODIFIED);
  GETXY(x1, y1, 1);
  GETXY(x2, y2, 3);
  CANVASXY(x1, y1);
  CANVASXY(x2, y2);
  //minor adjustment to coordinates
  x2 += x2 >= x1;
  x1 += x1 >= x2;
  y2 += y2 >= y1;
  y1 += y1 >= y2;
  DrawLine(x1, y1, x2, y2, selectedColor);
  return 0;
}

int lfloodFill(lua_State *L) {
  GETXY(x, y, 1);
  CANVASXY(x, y);
  if(x < 0 || y < 0 || x >= canvas.texture.width || y >= canvas.texture.height)
    return 0;
  undoCanvas(MODIFIED);
  floodFill(x,y); // calls scanLineFill
  return 0;
}

int lcopyRect(lua_State *L) {
  UnloadTexture(clipboard.region);
  Image tmp = GetTextureData(canvas.texture);
  Rectangle region = getRectOnCanvas(L);
  //flip region vertically
  region.y = canvas.texture.height - region.y - region.height;
  ImageCrop(&tmp, region);
  ImageFlipVertical(&tmp);
  ImageColorReplace(&tmp, backgroundColor, BLANK);
  clipboard.region = LoadTextureFromImage(tmp);
  clipboard.sourceRec.width = clipboard.region.width;
  clipboard.sourceRec.height = clipboard.region.height;
  clipboard.rotation = 0;
  UnloadImage(tmp);
  return 0;
}

int lshowCopiedRegion(lua_State *L) {
  EndTextureMode();
  GETXY(x,y,1);
  //align to pixel grid
  x -= (x - (int)canvasPos.x) % (int)zoom;
  y -= (y - (int)canvasPos.y) % (int)zoom;
  DrawTexturePro(clipboard.region, clipboard.sourceRec,
		 (Rectangle){x,y, clipboard.region.width * zoom, clipboard.region.height * zoom},
		 (Vector2){clipboard.region.width/2 * zoom, clipboard.region.height/2 * zoom},//origin = center
		 clipboard.rotation, WHITE);
  BeginTextureMode(canvas);
  return 0;
}

int lpaste(lua_State *L) {
  undoCanvas(MODIFIED);
  GETXY(x,y, 1);
  CANVASXY(x,y);
  DrawTexturePro(clipboard.region, clipboard.sourceRec,
		 (Rectangle){x,y, clipboard.region.width, clipboard.region.height},
		 (Vector2){clipboard.region.width/2, clipboard.region.height/2},//origin = center
		 clipboard.rotation, WHITE);
  return 0;
}

int LflipCregionClosure(lua_State *L) {
  enum {FLIP_HORIZONTAL = 1, FLIP_VERTICAL} flip = lua_tointeger(L, lua_upvalueindex(1));
  switch(flip) {
  case FLIP_HORIZONTAL:
    clipboard.sourceRec.width = -clipboard.sourceRec.width;
    break;
  case FLIP_VERTICAL:
    clipboard.sourceRec.height = -clipboard.sourceRec.height;
  }
  return 0;
}

int lflipCregionCommand(lua_State *L) {
  lua_pushcclosure(L, LflipCregionClosure, 1);
  return 1;
}

int LrotateCregionClosure(lua_State *L) {
  clipboard.rotation += lua_tointeger(L, lua_upvalueindex(1));
  return 0;
}

int lrotateCregionCommand(lua_State *L) {
  lua_pushcclosure(L, LrotateCregionClosure, 1);
  return 1;
}

/**************************functions used by Lua: END*/

static const luaL_Reg l[] = {
  {"rename", lrename},
  {"open", lopen},
  {"newImage", lnewImage},
  {"save", lsave},
  {"setGrid", lsetGrid},
  {"move", lmove},
  {"moveCommand", lmoveCommand},
  {"zoom", lzoom},
  {"zoomCommand", lzoomCommand},
  {"undo", lundo},
  {"saveMousePos", lsaveMousePos},
  {"showRectOutline", lshowRectOutline},
  {"drawRectOutline", ldrawRectOutline},
  {"drawRect", ldrawRect},
  {"drawLine", ldrawLine},
  {"floodFill", lfloodFill},
  {"copyRect", lcopyRect},
  {"showCopiedRegion", lshowCopiedRegion},
  {"paste", lpaste},
  {"flipCregionCommand", lflipCregionCommand},
  {"rotateCregionCommand", lrotateCregionCommand},
  {NULL, NULL}
};

//used to display all the palette colors and the selected and secondary one
void drawColorCell(int x, int y, Color color,int size) {
  DrawRectangle(GetScreenWidth() - (x + 1) * size, y * size + CELL_SPACING,
                size - CELL_SPACING, size - CELL_SPACING, color);
}

void swapColor(Color *color) {
  Color tmp = selectedColor;
  selectedColor = *color;
  *color = tmp;
}

void pushConstants(lua_State *L, char const *names[], int values[]) {
  for(int i=0; names[i]; ++i) {
    lua_pushinteger(L, values[i]);
    lua_setglobal(L, names[i]);
  }
}

int main(int argc, char *argv[]) {
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  luaL_newlib(L, l);
  lua_setglobal(L, "rpx");
  pushConstants(L, (char const*[])
		{"MOUSE_PRESSED", "MOUSE_DOWN", "MOUSE_RELEASED",
		  "FLIP_HORIZONTAL", "FLIP_VERTICAL", NULL},
		(int[]){1,2,3,1,2});
  char configHome[CONF_HOME_LEN + 1];
  strncpy(configHome, getenv("HOME"), CONF_HOME_LEN);
  strncat(configHome, "/.config/raypixed/conf.lua",
	  CONF_HOME_LEN - strlen(configHome));
  if (luaL_dofile(L, FileExists(configHome) ? configHome : "conf.lua"))
    error_lua(L, lua_tostring(L, -1));
  int commandKeyRefs[NCHARS] = {0}, toolKeys[NCHARS] = {0};
  struct palette palette = loadPalette(L);
  struct toolbar toolbar = loadTools(L, toolKeys);
  selectedColor = palette.colors[0];
  Color otherColor = backgroundColor = palette.colors[1];
  int selectedTool = 0, mouseButton = MOUSE_LEFT_BUTTON;
  enum {
    MOUSE_UP,
    MOUSE_PRESSED,
    MOUSE_DOWN,
    MOUSE_RELEASED
  } mouseState = MOUSE_UP;
  loadCommands(L, commandKeyRefs);
  InitWindow(SCREEN_W, SCREEN_H, "raypixed");
  SetTargetFPS(60);
  if(argc == 2) openImage(argv[1]);
  else {
    canvas = loadCanvas(L);
    BeginTextureMode(canvas);
    ClearBackground(backgroundColor);
    EndTextureMode();
  }
  //setting the initial state to savedCanvas
  undoCanvas(MODIFIED);
  undoCanvas(RESET);
  while (!WindowShouldClose()) {
    if(IsFileDropped()) {
      openImage(*GetDroppedFiles(NULL));
      ClearDroppedFiles();
    }
    /****************keyboard handling:START*/
    int k = GetCharPressed();
    if(prompt.active) {
      if (k && prompt.len < PROMPT_MAX_LEN) prompt.input[prompt.len++] = k;
      if(IsKeyPressed(KEY_ENTER)) {
	prompt.active = false;
	prompt.len = 0;
	prompt.onenter(prompt.input);
      } else if(IsKeyPressed(KEY_BACKSPACE)) {
	if(prompt.len) --prompt.len;
	else { //quit from prompt
	  prompt.active = false;
	  prompt.len = 0;
	}
      }
      prompt.input[prompt.len] = 0;
    } else if(' ' <= k && k <= '~') {
      k -= ' '; // treat as index
      if (toolKeys[k]) selectedTool = toolKeys[k] - 1;
      if (commandKeyRefs[k]) {
	// gets function from register
	lua_rawgeti(L, LUA_REGISTRYINDEX, commandKeyRefs[k]);
	BeginTextureMode(canvas);
	if (lua_pcall(L, 0, 0, 0)) // call with no args
	  error_lua(L, lua_tostring(L, -1));
	EndTextureMode();
      }
    }
    /****************keyboard handling:END*/

    /****************mouse handling:START*/
    palette.y += GetMouseWheelMove()*2; // to scroll the palette
    if(palette.y > 0) palette.y = 0;
    mousePos = GetMousePosition();
    switch(mouseState) {
    case MOUSE_UP:
      if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
	mouseButton = MOUSE_LEFT_BUTTON;
      else if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON))
	mouseButton = MOUSE_RIGHT_BUTTON;
      if (IsMouseButtonPressed(mouseButton)) {
	if (mousePos.x < toolbar.width && /*selecting a tool?*/
	    mousePos.y < toolbar.numTools * FONT_SIZE)
	  selectedTool = mousePos.y / FONT_SIZE;
	else if (mousePos.x > GetScreenWidth() - palette.width && /*selecting a color?*/
		 mousePos.y < (palette.numColors+palette.y) * palette.width) {
	  int i = ((int)mousePos.y / palette.width) - palette.y;
	  if(mouseButton == MOUSE_LEFT_BUTTON) selectedColor = palette.colors[i];
	  else otherColor = palette.colors[i];
	} else {
	  if(mouseButton == MOUSE_RIGHT_BUTTON)
	    swapColor(&otherColor);
	  mouseState = MOUSE_PRESSED;
	  //the position of the mouse is saved here for convenience
	  savedPos = mousePos;
	}
      }
      break;
    case MOUSE_PRESSED: mouseState = MOUSE_DOWN; break;
    case MOUSE_DOWN:
      if(IsMouseButtonReleased(mouseButton))
	mouseState = MOUSE_RELEASED;
      break;
    case MOUSE_RELEASED:
      mouseState = MOUSE_UP;
      if(mouseButton == MOUSE_RIGHT_BUTTON)
	swapColor(&otherColor);
      undoCanvas(RESET);
    }
    /****************mouse handling:END*/
    BeginDrawing();
    ClearBackground(GRAY);
    DrawTexturePro(canvas.texture, (Rectangle){0,0, canvas.texture.width,-canvas.texture.height},//vertically flipped
		   (Rectangle){canvasPos.x, canvasPos.y, canvas.texture.width * zoom, canvas.texture.height * zoom},
		   (Vector2){0}, 0, WHITE);
    DrawRectangleLines(canvasPos.x, canvasPos.y, canvas.texture.width * zoom, canvas.texture.height * zoom, WHITE);
    if (mouseState) {
      lua_rawgetp(L, LUA_REGISTRYINDEX, toolbar.tools + selectedTool);
      lua_pushinteger(L, mouseState);
      lua_pushinteger(L, mousePos.x);
      lua_pushinteger(L, mousePos.y);
      lua_pushinteger(L, savedPos.x);
      lua_pushinteger(L, savedPos.y);
      BeginTextureMode(canvas);
      if (lua_pcall(L, 5, 0, 0))
	error_lua(L, lua_tostring(L, -1));
      EndTextureMode();
    }
    if(grid.enabled) { // draw grid
      for(float x = canvasPos.x; x < canvasPos.x + canvas.texture.width * zoom; x += grid.width * zoom)
	DrawLine(x, canvasPos.y, x, canvasPos.y + canvas.texture.height * zoom, LIGHTGRAY);
      for(float y = canvasPos.y; y < canvasPos.y + canvas.texture.height * zoom; y += grid.height * zoom)
	DrawLine(canvasPos.x, y, canvasPos.x + canvas.texture.width * zoom, y, LIGHTGRAY);
    }
    // draw toolbar
    DrawRectangle(0, 0, toolbar.width, toolbar.numTools * FONT_SIZE,
		  (Color){0, 0, 0, 32});
    for (int i = 0; i < toolbar.numTools; ++i)
      DrawText(toolbar.tools[i], 0, i * FONT_SIZE, FONT_SIZE,
	       i == selectedTool ? WHITE : LIGHTGRAY);
    // draw clipboard preview
    DrawTexture(clipboard.region, 0, toolbar.numTools * FONT_SIZE, WHITE);
    if(prompt.active) { // draw prompt text
      DrawText(prompt.message, toolbar.width, 0, FONT_SIZE, WHITE);
      DrawText(prompt.input, toolbar.width, FONT_SIZE, FONT_SIZE, WHITE);
    } else DrawText(imageName, toolbar.width, 0, FONT_SIZE, WHITE); // or image name
    // draw color palette
    for (int i = 0; i < palette.numColors; ++i)
      drawColorCell(0, i + palette.y, palette.colors[i], palette.width);
    drawColorCell(1, 0, selectedColor, palette.width);
    drawColorCell(1, 1, otherColor, palette.width);
    EndDrawing();
  }
  UnloadRenderTexture(canvas);
  UnloadTexture(clipboard.region);
  undoCanvas(CLEAR);
  CloseWindow();
  free(palette.colors);
  free(toolbar.tools);
  lua_close(L);
  return 0;
}
