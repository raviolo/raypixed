default_width = 48
default_height = 48
colorcell_size = 48

palette = {0x00ff00ff, 0xeeeeeeff} --the second color is the transparent background
for i=0,15 do
   palette[#palette + 1] = string.format("0x%x%x0000ff", i, i) -- shades of red
   palette[#palette + 1] = string.format("0x00%x%x%x0ff",i, i, 15 - i, 15 - i) --from blue to green
end

function holdnrelease(hold, release)
   return function(mouseState, x, y, sx, sy)
      hold(x, y, sx, sy)
      if(mouseState == MOUSE_RELEASED) then release(x, y, sx, sy) end
   end
end

tools = {
   {function(m, x, y, sx, sy)
	 rpx.drawLine(x,y, sx, sy)
	 rpx.saveMousePos()
    end, name = "pencil", key = "p"},
   {holdnrelease(rpx.showRectOutline, rpx.drawRect), name = "rect"},
   {holdnrelease(rpx.showRectOutline, rpx.drawLine), name = "line"},
   {holdnrelease(rpx.showRectOutline, rpx.copyRect), name = "copy"},
   {holdnrelease(rpx.showCopiedRegion, rpx.paste), name = "paste"},
   {function(m, x, y, sx, sy)
	 rpx.move(x - sx, y - sy)
	 rpx.saveMousePos()
    end, name = "move"},
   {function(m, x, y)
	 if(m == MOUSE_PRESSED) then rpx.floodFill(x,y) end
    end, name = "bucket"}
}

speed = 64
commands = {
   h = rpx.moveCommand(speed, 0),
   j = rpx.moveCommand(0, -speed),
   k = rpx.moveCommand(0, speed),
   l = rpx.moveCommand(-speed, 0),
   ["+"] = rpx.zoomCommand(1),
   ["-"] = rpx.zoomCommand(-1),
   ["("] = rpx.rotateCregionCommand(-15),
   [")"] = rpx.rotateCregionCommand(15),
   f = rpx.flipCregionCommand(FLIP_HORIZONTAL),
   v = rpx.flipCregionCommand(FLIP_VERTICAL),
   u = rpx.undo,
   g = rpx.setGrid,
   r = rpx.rename,
   n = rpx.newImage,
   o = rpx.open,
   s = rpx.save,
}
